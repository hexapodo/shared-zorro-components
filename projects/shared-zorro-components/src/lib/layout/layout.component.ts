import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'custom-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    @Input() menuOptions = [];

    leftDrawerShow = false;

    leftDrawerStyle = {
        padding: '0'
    };

    constructor() { }

    ngOnInit(): void {
    }

    openLeftDrawer() {
        this.leftDrawerShow = true;
    }

    closeLeftDrawer() {
        this.leftDrawerShow = false;
    }

}
