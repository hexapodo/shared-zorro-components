import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    menuOptions = [
        {
            title: 'group 1',
            icon: 'home',
            items: [
                {
                    title: 'Users Management',
                    route: ['/home']
                },
                {
                    title: 'option 2',
                    route: ['/home2']
                },
                {
                    title: 'option 3',
                    route: ['/home3']
                }
            ]
        },
        {
            title: 'group 2',
            icon: 'user',
            items: [
                {
                    title: 'option 4',
                    route: ['/home4']
                }
            ]
        }
    ];
}
