import { Component, OnInit, Input } from '@angular/core';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

    @Input() value;
    constructor(
        private drawerRef: NzDrawerRef<string>
    ) { }

    ngOnInit(): void {
    }

    close(): void {
        this.drawerRef.close(this.value);
    }

}
