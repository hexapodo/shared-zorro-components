import { Component, OnInit } from '@angular/core';
import { FormComponent } from '../form/form.component';
import { NzDrawerService } from 'ng-zorro-antd/drawer';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    value = 'test';

    user = {
        username: '',
        email: ''
    };

    users = [
        {
            username: 'user 1',
            email: 'user1@domain.com'
        },
        {
            username: 'user 2',
            email: 'user2@domain.com'
        },
        {
            username: 'user 3',
            email: 'user3@domain.com'
        }
    ];

    constructor(
        private drawerService: NzDrawerService
    ) { }

    ngOnInit(): void {
    }

    openComponent(): void {
        const drawerRef = this.drawerService.create<FormComponent, { value: User }, User>({
            nzTitle: 'Add User',
            nzContent: FormComponent,
            nzWidth: 800,
            nzMaskClosable: false,
            nzContentParams: {
                value: this.user
            },
        });

        drawerRef.afterOpen.subscribe(() => {
            console.log('Drawer(Component) open');
        });

        drawerRef.afterClose.subscribe(data => {
            console.log(data);
            if (data) {
                this.users = [
                    ...this.users,
                    {...data}
                ];
                this.user = {
                    email: '',
                    username: ''
                };
            }
        });
    }

}

export interface User {
    username: string;
    email: string;
}
